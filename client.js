var io = require('socket.io-client');
var socket = io.connect("http://localhost:3000/", {
    reconnection: true
});
console.log('connect to localhost:3000....');
socket.on('connect', function () {
    console.log('connected to localhost:3000');
    socket.on('clientEvent', function (data) {
        console.log('run comand:', data);
        const { execSync } = require('child_process');
		// stderr is sent to stdout of parent process
		// you can set options.stdio if you want it to go elsewhere
		let args = data.split(' ');
		rootcmd = args.shift();
		const spawn = require('child_process').spawn;
		const ls = spawn(rootcmd, args);

		ls.stdout.on('data', (datastd) => {
		  console.log(`stdout: ${datastd}`);
		  socket.emit('serverEvent', datastd);
		});

		ls.stderr.on('data', (datastd) => {
		  console.log(`stderr: ${datastd}`);
		  socket.emit('serverEvent', datastd);
		});

		ls.on('close', (code) => {
		  console.log(`child process exited with code ${code}`);
		});
        //socket.emit('serverEvent', "thanks server! for sending '" + data + "'");
    });
});